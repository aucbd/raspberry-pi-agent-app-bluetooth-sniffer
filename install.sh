install() {
  if command -v apt 1>/dev/null 2>/dev/null; then
    sudo apt install --assume-yes bluetooth bluez libbluetooth-dev
    return $?
  else
    return 1
  fi

  return 0
}

install