from time import perf_counter
from time import sleep
from typing import Any
from typing import List
from typing import Tuple

from bluetooth import discover_devices
from bluetooth.ble import DiscoveryService


def app(settings: 'Settings', log: 'Log', agent: 'Agent'):
    service_ble = DiscoveryService()

    while True:
        cycle_start = perf_counter()
        devices: List[Tuple[Any, ...]] = []
        devices_ble: dict = {}

        while perf_counter() - cycle_start < settings["bluetooth_sniffer"]["transmission_interval"]:
            agent.loop(0.1)
            devices.extend(discover_devices(settings["bluetooth_sniffer"]["read_time"]))
            if settings["bluetooth_sniffer"]["ble"]:
                devices_ble.update(service_ble.discover(settings["bluetooth_sniffer"]["read_time"]))

        devices_total: set = set(devices + [adr for adr in devices_ble])
        log.write(f"BLUETOOTH SNIFFER:DEVICES {len(devices_total)}")

        agent.loop(0.1)
        agent.publish("bluetooth_devices", len(devices_total))
